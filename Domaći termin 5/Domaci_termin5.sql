-- MySQL dump 10.13  Distrib 5.7.9, for linux-glibc2.5 (x86_64)
--
-- Host: localhost    Database: Domaci_Milorad_Mirkovic
-- ------------------------------------------------------
-- Server version	5.6.33-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Kupac`
--

DROP TABLE IF EXISTS `Kupac`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Kupac` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Ime` varchar(40) COLLATE utf8_bin NOT NULL,
  `Prezime` varchar(40) COLLATE utf8_bin NOT NULL,
  `Adresa` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Kupac`
--

LOCK TABLES `Kupac` WRITE;
/*!40000 ALTER TABLE `Kupac` DISABLE KEYS */;
INSERT INTO `Kupac` VALUES (1,'Aleksandra','Mirkovic','Kraljevica Marka 30'),(2,'Natasa ','Diklic',''),(3,'Lazar ','Diklic','Jozefa Marcoka 30');
/*!40000 ALTER TABLE `Kupac` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Narudžbenica`
--

DROP TABLE IF EXISTS `Narudžbenica`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Narudžbenica` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Datum` date NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Narudžbenica`
--

LOCK TABLES `Narudžbenica` WRITE;
/*!40000 ALTER TABLE `Narudžbenica` DISABLE KEYS */;
INSERT INTO `Narudžbenica` VALUES (1,'2022-09-12'),(2,'2022-09-06'),(3,'2022-06-15'),(4,'2022-07-01');
/*!40000 ALTER TABLE `Narudžbenica` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Proizvod`
--

DROP TABLE IF EXISTS `Proizvod`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Proizvod` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Naziv` varchar(50) COLLATE utf8_bin NOT NULL,
  `Barkod` varchar(13) CHARACTER SET utf8 NOT NULL,
  `Cijena` decimal(6,2) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Proizvod`
--

LOCK TABLES `Proizvod` WRITE;
/*!40000 ALTER TABLE `Proizvod` DISABLE KEYS */;
INSERT INTO `Proizvod` VALUES (1,'Kikiriki','trikatmskutiq',199.99),(2,'Pivo','jimkrapskeqwr',100.00),(3,'Smoki','qelmtnaqrvbrt',90.00);
/*!40000 ALTER TABLE `Proizvod` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-09-15 20:21:13
